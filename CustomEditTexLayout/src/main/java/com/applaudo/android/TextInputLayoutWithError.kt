package com.applaudo.android

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputLayout

class TextInputLayoutWithError(
    context: Context,
    attrs: AttributeSet?
) : TextInputLayout(context, attrs) {

    private var hintText: String?
    private var hintTextColor: Int
    private var secondHintTextColor: Int
    private var errorColor: Int
    private var errorText: String? = null
    private val backgroundColor: Int
    private val textColor: Int
    private val strokeWidth: Int

    init {
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.TextInputLayoutWithError)
        attributes.apply {
            backgroundColor = getColor(
                R.styleable.TextInputLayoutWithError_background_color,
                ContextCompat.getColor(context, android.R.color.darker_gray)
            )
            errorColor = getColor(
                R.styleable.TextInputLayoutWithError_error_text_color,
                ContextCompat.getColor(context, android.R.color.holo_red_light)
            )
            hintTextColor = getColor(
                R.styleable.TextInputLayoutWithError_hint_text_color,
                ContextCompat.getColor(context, android.R.color.white)
            )
            secondHintTextColor = getColor(
                R.styleable.TextInputLayoutWithError_second_hint_text_color,
                ContextCompat.getColor(context, android.R.color.darker_gray)
            )
            textColor = getColor(
                R.styleable.TextInputLayoutWithError_text_color,
                ContextCompat.getColor(context, android.R.color.black)
            )
            hintText = getString(R.styleable.TextInputLayoutWithError_hint_text)

            strokeWidth = getDimensionPixelSize(
                R.styleable.TextInputLayoutWithError_stroke_width,
                DEFAULT_STROKE_WIDTH
            )
            recycle()
        }
    }

    fun setCustomErrorText(message: String?) {
        errorText = message
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (editText?.isFocused == true || editText?.text.isNullOrBlank().not()) {
            boxBackgroundColor = ContextCompat.getColor(context, android.R.color.transparent)
            boxStrokeWidthFocused = strokeWidth
            boxStrokeWidth = strokeWidth

            if (errorText.isNullOrEmpty().not()) {
                hint = errorText
                defaultHintTextColor = ColorStateList.valueOf(errorColor)
                editText?.setTextColor(errorColor)
            } else {
                hint = hintText
                defaultHintTextColor = ColorStateList.valueOf(secondHintTextColor)
                editText?.setTextColor(textColor)
            }
        } else {
            boxStrokeWidthFocused = 0
            boxStrokeWidth = 0
            boxBackgroundColor = backgroundColor
            defaultHintTextColor = ColorStateList.valueOf(hintTextColor)
            hint = hintText
        }
    }

    companion object {
        private const val DEFAULT_STROKE_WIDTH = 2
    }
}